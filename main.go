package main

import (
	"bufio"
	"context"
	"encoding/base64"
	firebase "firebase.google.com/go"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"firebase.google.com/go/messaging"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

const SCOPE = "https://www.googleapis.com/auth/cloud-platform"

var fcm *messaging.Client

func sendPush(token string, url string) {
	push := &messaging.Message{
		Token: token,
		Data: map[string]string{
			"urls": url,
		},
	}

	_, err := fcm.Send(context.Background(), push)
	if err != nil {
		log.Print("Error when sending push:", err)
		return
	}
	log.Print("Push send")
}

var r = rand.New(rand.NewSource(time.Now().UnixNano()))

func startDownload(url string, token string) {
	log.Print("Starting download " + url)

	rnd := strconv.Itoa(r.Int())
	tempdir := "tmp/out/" + rnd + "/"
	tempurl := "https://dev.czekanski.info/dl/" + rnd + "/"

	err := os.Mkdir(tempdir, 0755)
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Print("Temp dir: " + tempdir)

	cmd := exec.Command("youtube-dl", "-i", "--add-metadata", "--extract-audio", "--audio-format", "mp3", "-o", tempdir+"%(title)s.%(ext)s", url)
	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, "LC_ALL=en_US.UTF-8")
	log.Print(cmd.Args)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Print(err.Error())
		return
	}
	rd := bufio.NewReader(stdout)
	err = cmd.Start()

	if err != nil {
		log.Print(err.Error())
		return
	}

	regex := regexp.MustCompile("Adding metadata to '(.*)'")
	for {
		line, err := rd.ReadString('\n')
		if err != nil {
			log.Print(err.Error())
			return
		}
		out := regex.FindStringSubmatch(line)
		if len(out) == 2 {
			file := strings.Replace(out[1], tempdir, "", 1)
			pushurl := tempurl + file
			log.Print(pushurl)

			sendPush(token, pushurl)
		}
	}

	err = cmd.Wait()
	log.Print("Download finished")
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Query().Get("y")
	token := r.Header.Get("token")

	if len(url) == 0 {
		log.Println("No y param ginven")
		http.Error(w, "No y param given", 400)
		return
	}
	if len(token) == 0 {
		log.Println("No token given")
		http.Error(w, "No token given", 400)
		return
	}

	url = url[strings.Index(url, "http"):len(url)]

	go startDownload(url, token)
}

func main() {
	log.Print("YoutubeConv server")

	credentialsEnv := os.Getenv("GOOGLE_CREDENTIALS")
	if len(credentialsEnv) == 0 {
		log.Fatal("No GOOGLE_CREDENTIALS given")
		return
	}

	credentialsJson, err := base64.StdEncoding.DecodeString(credentialsEnv)
	if err != nil {
		log.Fatal(err)
		return
	}

	ctx := context.Background()
	credentials, err := google.CredentialsFromJSON(ctx, credentialsJson, SCOPE)
	if err != nil {
		log.Fatal(err)
		return
	}

	opt := option.WithCredentials(credentials)
	firebaseApp, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatal(err)
		return
	}

	fcm, err = firebaseApp.Messaging(ctx)
	if err != nil {
		log.Fatal(err)
		return
	}

	http.HandleFunc("/", mainHandler)
	err = http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
}
