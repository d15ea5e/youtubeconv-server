module bitbucket.org/d15ea5e/youtubeconv-server

go 1.13

require (
	firebase.google.com/go v3.11.0+incompatible
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6
	google.golang.org/api v0.15.0
)
