FROM golang:1.13-alpine as build
WORKDIR /app
RUN apk add --no-cache git ca-certificates
COPY . .
RUN CGO_ENABLED=0 go build .

FROM alpine:3.11
RUN apk add --no-cache youtube-dl ffmpeg ca-certificates
COPY --from=build /app/youtubeconv-server /usr/local/bin
ENTRYPOINT ["youtubeconv-server"]
